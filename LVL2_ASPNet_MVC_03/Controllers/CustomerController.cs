﻿using LVL2_ASPNet_MVC_03.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        db_customer2Entities DbModel = new db_customer2Entities();
        // GET: Customer
        public ActionResult Index()
        {
            return View(DbModel.tbl_customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(DbModel.tbl_customer.Where(x => x.Id == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(tbl_customer customer)
        {
            try
            {
                // TODO: Add insert logic here

                DbModel.tbl_customer.Add(customer);
                DbModel.SaveChanges();

                /* tbl_customer add = new tbl_customer
                 {
                     Name = customer.Name,
                     Phone_number = customer.Phone_number,
                     Email = customer.Email
                 };

                 DbModel.Entry(add).State = EntityState.Added;
                 DbModel.SaveChanges();*/

                return RedirectToAction("Index");
            }
            catch (Exception msgErr) //atau DivideByZeroException untuk general
            {
                ViewBag.Eror = msgErr.Message;
                //throw;
                return View();
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(DbModel.tbl_customer.Where(x=> x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, tbl_customer customer)
        {
            try
            {
                // TODO: Add update logic here
                DbModel.Entry(customer).State = EntityState.Modified;
                DbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(DbModel.tbl_customer.Where(x => x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tbl_customer customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = DbModel.tbl_customer.Where(x => x.Id == id).FirstOrDefault();
                DbModel.tbl_customer.Remove(customer);
                DbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
